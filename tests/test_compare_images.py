import pytest
import matplotlib.pyplot as plt
import os
import inspect
from helper.global_constant import *
from helper.CommonMethods import CommonMethods
from datetime import datetime

cm = CommonMethods()
grayscale_reference_image1 = cm.load_and_convert_image_to_grayscale(REFERENCE_IMAGE1_PATH)
grayscale_reference_image2 = cm.load_and_convert_image_to_grayscale(REFERENCE_IMAGE2_PATH)

@pytest.fixture
def load_image():
   height, width = cm.load_reference_image(REFERENCE_IMAGE1_PATH)
   yield height, width

def test_import_and_export_image_to_jpg(load_image):
    # Verify the reference image1 dimensions to be correct
    height, width = load_image
    assert height == 1080, "Image height is not valid"
    assert width == 1920, "Image width is not valid"
    print(f"Image dimensions: Height = {height}, Width = {width}")

    # Verify source image extension to be .png
    expected_extension = '.png'
    cm.verify_image_extension(REFERENCE_IMAGE1_PATH, expected_extension)

    # Convert an image to jpg format
    cm.export_image_to_jpg_format(REFERENCE_IMAGE1_PATH, OUTPUT_IMAGE_PATH)
    assert os.path.exists(OUTPUT_IMAGE_PATH), "Exported image does not exist"

    # Verify the newly saved .jpg image dimensions to be the same
    height, width = cm.load_reference_image(OUTPUT_IMAGE_PATH)
    assert height == 1080, "Image height is not valid"
    assert width == 1920, "Image width is not valid"
    print(f"Image dimensions: Height = {height}, Width = {width}")

def test_exported_image_with_reference_image2():
   # Export .png image to .jpg
   cm.export_image_to_jpg_format(REFERENCE_IMAGE1_PATH, OUTPUT_IMAGE_PATH)
   assert os.path.exists(OUTPUT_IMAGE_PATH), "Exported image does not exist"

   # Verify the difference between the exported image (.jpg) and reference image2 (.png)
   grayscale_output_image = cm.load_and_convert_image_to_grayscale(OUTPUT_IMAGE_PATH)
   mean_squared_error, image_differences = cm.compute_mse_between_two_images(grayscale_output_image, grayscale_reference_image2)
   assert (mean_squared_error) > 0, 'Both images are matching!'

   # Plot the result on grid
   get_current_framename = inspect.currentframe().f_code.co_name
   cm.plot_image(221, image_differences, get_current_framename + ' \nmse= ' + str(mean_squared_error))

def test_two_identical_images():
   # Verify the difference between two identical images (.png)
   mean_squared_error, image_differences = cm.compute_mse_between_two_images(grayscale_reference_image1, grayscale_reference_image1)
   assert (mean_squared_error) == 0, 'Both images are not matching!'

   # Plot the result on grid
   get_current_framename = inspect.currentframe().f_code.co_name
   cm.plot_image(223, image_differences, get_current_framename + ' \nmse= ' + str(mean_squared_error))

def teardown_function():
   output_path = os.path.join(os.path.dirname('test-results\\'), os.path.basename(__file__) + '_' + datetime.now().strftime("%Y-%m-%d-%H-%M") + '.png')
   plt.savefig(output_path)
   print(f"Plot saved to {output_path}")