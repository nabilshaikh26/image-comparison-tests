# image-comparison-tests

This python-based sample test project is implemented with the help of Pytest and OpenCV. This aims to compare and test the images, and report the differences using Matplotlib library.

## Folder Structure:

```
├── helper (this helps to encapsulate all the commonly used methods across all tests in a single place)
├── images (contains images that needs to be tested)
├── test-results (contains all the tests results in .png format)
├── tests (All test goes here)
|   └── test_compare_images.py 
├── requirements.txt (contains all the external libraries and their versions that the project depends on)
```

## Installation:

- Clone the project.
- Open terminal, and run following commands:
    - Install [pip](https://pip.pypa.io/en/stable/installation/#ensurepip) package manager (if it is not installed on your machine)
        - `py -m ensurepip --upgrade` (Windows) or
        - `python -m ensurepip --upgrade` (MacOS)
    - Install project-related dependencies
        - `pip install -r requirements.txt`

<right><p align="right">(<a href="#image-comparison-tests">back to top</a>)</p></right>

## Running Tests:

Once the project is cloned and configured, go to terminal and run the following command to execute the tests.

- `python -m pytest`

Once the execution is complete, this is how the test report would look like,

<ins>Report</ins>:

<kbd><img src="/uploads/adb687c8db7976eb1ae1e88a2fadb4fb/report.PNG" alt="Report" border="1" width=800></kbd>

<right><p align="right">(<a href="#image-comparison-tests">back to top</a>)</p></right>

## Contact

<a href="mailto:nabilshaikh26@gmail.com"><img src="https://img.shields.io/badge/Gmail-D14836?style=for-the-badge&logo=gmail&logoColor=white" /> &nbsp; <a href="https://www.github.com/nabilshaikh"><img src="https://img.shields.io/badge/GitHub-100000?style=for-the-badge&logo=github&logoColor=white"/> &nbsp; <a href="https://www.gitlab.com/nabilshaikh26"><img src="https://img.shields.io/badge/GitLab-330F63?style=for-the-badge&logo=gitlab&logoColor=white"/> &nbsp; <a href="https://www.linkedin.com/in/nabil-shaikh-5362b71b3/"><img src="https://img.shields.io/badge/LinkedIn-0077B5?style=for-the-badge&logo=linkedin&logoColor=white"/>


<right><p align="right">(<a href="#image-comparison-tests">back to top</a>)</p></right>

## Acknowledgments

Following is the list of resources that you may find helpful:

<ul>
<li>[Pytest](https://docs.pytest.org/en/7.4.x/)</li>
<li>[OpenCV](https://docs.opencv.org/3.4/db/deb/tutorial_display_image.html)</li>
<li>[Matplotlib](https://matplotlib.org/stable/gallery/index.html)</li>
<li>[NumPy](https://numpy.org/devdocs/user/absolute_beginners.html)</li>
</ul>

<right><p align="right">(<a href="#image-comparison-tests">back to top</a>)</p></right>