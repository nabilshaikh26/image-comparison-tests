import cv2
import matplotlib.pyplot as plt
import numpy as np
import os

class CommonMethods:
    def load_and_convert_image_to_grayscale(self, image_path):
        image = cv2.imread(image_path)
        image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY) 
        return image
    
    def compute_mse_between_two_images(self, image1, image2):
        image1Height, image1Width = image1.shape
        imageDifferences = cv2.subtract(image1, image2)
        mse = (np.sum(imageDifferences ** 2)) / float(image1Height*image1Width)
        return mse, imageDifferences

    def plot_image(self, position, imageDifferences, title):
        return plt.subplot(position), plt.imshow(imageDifferences,'gray'),plt.title(title),plt.axis('off')

    def load_reference_image(self, image_path):
        image = cv2.imread(image_path)
        assert image is not None, f"Failed to load the image from {image_path}"
        height, width = image.shape[:2] # returns a tuple containing image dimensions (i.e. height and width)
        return height, width

    def verify_image_extension(self, image_path, expected_extension):
        # Check if the image file exists
        assert os.path.exists(image_path), f"Image file does not exist at {image_path}"
        # Extract the file extension
        _, file_extension = os.path.splitext(image_path)
        # Convert the extension to lowercase for case-insensitive comparison
        file_extension = file_extension.lower()
        # Verify if the actual extension matches the expected extension
        assert file_extension == expected_extension.lower(), f"Expected {expected_extension}, but found {file_extension}"

    def export_image_to_jpg_format(self, input_image_path, output_image_path):
        image = cv2.imread(input_image_path)
        # Check if the image is loaded successfully
        assert image is not None, f"Failed to load the image from {input_image_path}"
        # Save the image in JPG format
        cv2.imwrite(output_image_path, image, [cv2.IMWRITE_JPEG_QUALITY, 95])